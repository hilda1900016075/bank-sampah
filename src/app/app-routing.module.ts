import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegistrasiComponent } from './auth/registrasi/registrasi.component';

const routes: Routes = [
  {
    path:'login',
    component: LoginComponent
  },
  {
    path : 'registrasi',
    component: RegistrasiComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo:'/login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
